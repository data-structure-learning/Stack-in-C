//
//  main.c
//  Stack-in-C
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <stdio.h>
#include "Stack.h"

void testStack();

int main(int argc, const char * argv[]) {
    
    printf("testStack");
    testStack();
    
    return 0;
    
}

void testStack() {
    Stack s;
    
    InitStack(&s, 5);
    
    Push(&s, 2);
    Push(&s, 5);
    Push(&s, 7);
    
    StackTraverse(&s);
    
    int e = 0;
    Pop(&s, &e);
    printf("e: %d\n", e);
    
    StackTraverse(&s);
    
    Push(&s, 12);
    Push(&s, 19);
    Push(&s, 31);
    Push(&s, 50);
    
    StackTraverse(&s);
    
    
    Pop(&s, &e);
    Pop(&s, &e);
    Pop(&s, &e);
    Pop(&s, &e);
    Pop(&s, &e);
    Pop(&s, &e);
    
    StackTraverse(&s);
    
    Push(&s, 81);
    ClearStack(&s);
    
    StackTraverse(&s);
}
