//
//  Stack.h
//  Stack-in-C
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef Stack_h
#define Stack_h

#include <stdio.h>

typedef struct {
    
    int *m_pSatck;
    int m_iSatckCapacity;
    
    int m_iTop;
} Stack, *P_Stack;

void InitStack(P_Stack s, int stackCapacity);
void FreeStack(P_Stack s);
void ClearStack(P_Stack s);
int StackEmpty(P_Stack s);
int StackFull(P_Stack s);
int StackLength(P_Stack s);
int Push(P_Stack s, int element);
int Pop(P_Stack s, int *element);
void StackTraverse(P_Stack s);

#endif /* Stack_h */
