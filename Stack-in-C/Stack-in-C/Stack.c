//
//  Stack.c
//  Stack-in-C
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <stdlib.h>
#include "Stack.h"

void InitStack(P_Stack s, int stackCapacity) {
    s->m_pSatck = (int *)malloc(sizeof(int)*stackCapacity);
    
    if (s->m_pSatck == NULL) {
        exit(-1);
    }
    
    s->m_iSatckCapacity = stackCapacity;
    ClearStack(s);
}

void FreeStack(P_Stack s) {
    free(s->m_pSatck);
    s->m_pSatck = NULL;
}

void ClearStack(P_Stack s) {
    s->m_iTop = 0;
}

int StackEmpty(P_Stack s) {
    return s->m_iTop == 0;
}

int StackFull(P_Stack s) {
    return s->m_iTop >= s->m_iSatckCapacity;
}

int StackLength(P_Stack s) {
    return s->m_iTop;
}

int Push(P_Stack s, int element) {
    if (StackFull(s)) {
        return 0;
    }
    
    s->m_pSatck[s->m_iTop ++] = element;
    return 1;
}

int Pop(P_Stack s, int *element) {
    if (StackEmpty(s)) {
        return 0;
    }
    
    *element = s->m_pSatck[-- s->m_iTop];
    return 1;
}

void StackTraverse(P_Stack s) {
    for (int i = s->m_iTop-1; i >= 0 ; i --) {
        printf("%d ", s->m_pSatck[i]);
    }
    printf("\n");
}
